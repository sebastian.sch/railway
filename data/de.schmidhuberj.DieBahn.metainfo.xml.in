<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2022 Julian Schmidhuber -->
<component type="desktop-application">
  <id>@app-id@</id>
  <project_license>GPL-3.0-or-later</project_license>
  <metadata_license>FSFAP</metadata_license>
  <name>Railway</name>
  <summary>Find all your travel information</summary>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <content_rating type="oars-1.1"/>
  <developer_name>Julian Schmidhuber</developer_name>
  <developer id="mobile.schmidhuberj.de">
    <name>Julian Schmidhuber</name>
  </developer>
  <translation type="gettext">@gettext-package@</translation>
  <update_contact>flatpak@schmiddi.anonaddy.com</update_contact>

  <url type="homepage">https://mobile.schmidhuberj.de/railway</url>
  <url type="bugtracker">https://gitlab.com/schmiddi-on-mobile/railway/-/issues</url>
  <url type="translate">https://hosted.weblate.org/engage/schmiddi-on-mobile/</url>
  <url type="donation">https://gitlab.com/schmiddi-on-mobile/railway#donate</url>
  <url type="contact">https://matrix.to/#/#railwayapp:matrix.org</url>
  <url type="vcs-browser">https://gitlab.com/schmiddi-on-mobile/railway</url>

  <description>
    <p>
      Railway lets you look up travel information across networks and borders
      without having to navigate through different websites. Due to the adaptive
      design, it is suitable to plan your trip in advance or mobile on the go:
    </p>
    <ul>
      <li>
        View your tripsʼ details including platforms and delays
      </li>
      <li>
        Bookmark current and future trips as well as frequent search
      </li>
      <li>
        Make use of advance search options like filtering by mode of transport
      </li>
    </ul>
    <p>
      And all that for networks from all around the world, but mostly from Europe.
    </p>
  </description>

  <releases>
    <release version="2.3.0" date="2024-02-11">
      <description>
        <p>Added</p>
        <ul>
          <li>Keyboard navigation to station search.</li>
          <li>Label for frequency of trains.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>Strings used in the UI.</li>
          <li>Improved remarks for journey legs.</li>
          <li>Many minor UI improvements.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>For the station search, clear the place when the entry is cleared.</li>
        </ul>
      </description>
    </release>
    <release version="2.2.0" date="2023-12-11">
      <description>
        <p>Added</p>
        <ul>
          <li>Translation support using Weblate.</li>
          <li>Filter for providers.</li>
          <li>Indicate when journey was last refreshed.</li>
          <li>Save window height between startups.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>Improved UI for date-dividers.</li>
          <li>Disable search button while search is in progress.</li>
          <li>Disable refresh button while refreshing journey is in progress.</li>
          <li>Added timeout to all requests.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Don't collapse journey legs stopovers and remarks after refreshing.</li>
          <li>Build on i386.</li>
        </ul>
      </description>
    </release>
    <release version="2.1.0" date="2023-10-07">
      <description>
        <p>Added</p>
        <ul>
          <li>Keyboard shortcuts</li>
          <li>Date dividers between journeys.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>UI improvements on the preferences.</li>
          <li>Don't display walk destination, unless it is the final walk.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Spinners in the time dropdown not being two-digit sometimes.</li>
          <li>Duration of over one day being displayed incorrectly.</li>
          <li>The date and time cards having a different color than the rest when in dark mode.</li>
        </ul>
      </description>
    </release>
    <release version="2.0.0" date="2023-09-21">
      <description>
        <p>Complete UI overhaul.</p>
      </description>
    </release>
    <release version="1.5.0" date="2023-07-16">
      <description>
        <p>New icon.</p>
      </description>
    </release>
    <release version="1.4.1" date="2023-06-05">
      <description>
        <p>Made about- and preferences-windows transient.</p>
      </description>
    </release>
    <release version="1.4.0" date="2023-05-20">
      <description>
        <p>New icon</p>
      </description>
    </release>
    <release version="1.3.0" date="2023-03-28">
      <description>
        <p>UI improvements</p>
      </description>
    </release>
    <release version="1.2.2" date="2023-03-12">
      <description>
        <p>Fix problems with DB and other providers.</p>
      </description>
    </release>
    <release version="1.2.1" date="2022-10-04">
      <description>
        <p>Bug fixing for the new providers.</p>
      </description>
    </release>
    <release version="1.2.0" date="2022-10-01">
      <description>
        <p>Added support for different providers.</p>
      </description>
    </release>
    <release version="1.1.0" date="2022-08-17">
      <description>
        <p>Option to autodelete old journeys, small bug fix</p>
      </description>
    </release>
    <release version="1.0.0" date="2022-04-09">
      <description>
        <p>Many more settings</p>
      </description>
    </release>
    <release version="0.1.5" date="2022-04-03">
      <description>
        <p>Show prices, save searches between application starts, fix bugs</p>
      </description>
    </release>
    <release version="0.1.4" date="2022-03-29">
      <description>
        <p>Fix application freeze, dutch translation</p>
      </description>
    </release>
    <release version="0.1.3" date="2022-03-27">
      <description>
        <p>Fixed some api bugs</p>
      </description>
    </release>
    <release version="0.1.2" date="2022-03-26">
      <description>
        <p>Bookmark searches, more information on connections</p>
      </description>
    </release>
    <release version="0.1.1" date="2022-03-22">
      <description>
        <p>Bookmark journeys</p>
      </description>
    </release>
    <release version="0.1.0" date="2022-03-19">
      <description>
        <p>Initial Release.</p>
      </description>
    </release>
  </releases>

  <screenshots>
    <screenshot type="default">
      <caption>Application Overview</caption>
      <image>https://gitlab.com/schmiddi-on-mobile/railway/-/raw/master/data/screenshots/overview.png</image>
    </screenshot>
  </screenshots>

  <provides>
    <id>@app-id@</id>
    <binary>diebahn</binary>
  </provides>

  <supports>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </supports>
  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <internet>always</internet>
  </recommends>
  <requires>
    <display_length compare="ge" side="shortest">300</display_length>
    <display_length compare="ge" side="longest">360</display_length>
  </requires>
</component>

